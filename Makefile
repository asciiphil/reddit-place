.PHONY: all
all: place_pb2.py

place_pb2.py: place.proto
	protoc -I . --python_out . $<
