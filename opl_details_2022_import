#!/usr/bin/env python3

import csv
import datetime
import lzma
import pathlib
import re
import sqlite3
import time

import click
import numpy as np
import pandas as pd
import sqlalchemy as db
from tqdm import tqdm, trange
import ujson

from common import TimedAction, linecount, COLORS, dt_to_ts, psql_insert_copy


STRIDE = 10000


def validate_table_name(ctx, param, value):
    if re.search(r'^[a-z0-9_]+$', value) is None:
        raise click.BadParameter('Invalid table name')
    return value


@click.command()
@click.help_option('-h', '--help')
@click.option('-t', '--table', default='opl_details', show_default=True,
              callback=validate_table_name)
@click.argument('database')
@click.argument('details', nargs=-1, type=click.Path(exists=True))
def main(details, database, table):
    """Reads the detail files collected by u/opl_ and stores the username and
    placement information in an HDF5 table.

    The database should be given as an SQLAlchemy connection string.

    """
    details = [pathlib.Path(p) for p in details]
    total_detail_size = sum(d.stat().st_size for d in details)
    progress_bar = tqdm(total=total_detail_size, desc='files', position=0,
                        unit_scale=True, unit='B')
    dfs = []
    for detail_file in details:
        dfs.append(import_file(detail_file))
        progress_bar.update(detail_file.stat().st_size)
    with TimedAction('Concatenating DataFrames'):
        df = pd.concat(dfs, ignore_index=True).drop_duplicates()
    del dfs
    print(df.head())
    with TimedAction('Sorting records'):
        sort_columns = ['timestamp', 'y', 'x']
        order = np.lexsort(
            [df[col].values for col in reversed(sort_columns)])
        for col in list(df.columns):
            df[col] = df[col].values[order]
    with TimedAction('Converting user hashes to category'):
        df['username'] = df['username'].astype('category')
    with TimedAction('Assigning index'):
        df.set_index('timestamp', inplace=True)
    print(df.head())
    save_sql(df, database, table)
    

def import_file(path):
    coord_tuples = {
        'timestamp': [],
        'x': [],
        'y': [],
        'username': [],
    }
    csv.field_size_limit(2 * 1024 * 1024)
    with lzma.open(path, 'rt') as input_file:
        reader = csv.DictReader(
            input_file, fieldnames=['timestamp', 'label', 'author', 'data'],
            doublequote=False, escapechar='\\')
        for row in tqdm(reader, desc=path.stem, position=1, leave=None,
                        total=linecount(path, 1), unit='row'):
            if row['label'] != 'place2.details':
                continue
            try:
                import_detail_data(ujson.loads(row['data'])['data'], coord_tuples)
            except ValueError as e:
                # Ignore but log
                tqdm.write(str(row))
                tqdm.write(str(e))
    df = pd.DataFrame.from_records(coord_tuples)
    df.timestamp = df.timestamp.astype(np.int64)
    df.x = df.x.astype(np.uint16)
    df.y = df.y.astype(np.uint16)
    return df.drop_duplicates()


def import_detail_data(data, coord_tuples):
    if data is None:
        return
    for coords, coord_data in data.items():
        sep_index = coords.index('x')
        c_index = coords.find('c')
        x = int(coords[1:sep_index])
        if c_index == -1:
            y = int(coords[sep_index+1:])
        else:
            y = int(coords[sep_index+1:c_index])
        coord_data_tuples(x, y, coord_data, coord_tuples)
        

def coord_data_tuples(x, y, coord_data, coord_list):
    for placement in coord_data['data']:
        if placement['data']['lastModifiedTimestamp'] is None or placement['data']['userInfo'] is None:
            continue
        coord_list['timestamp'].append(int(placement['data']['lastModifiedTimestamp']))
        coord_list['x'].append(x)
        coord_list['y'].append(y)
        coord_list['username'].append(placement['data']['userInfo']['username'])


def save_sql(dataset, database, table):
    engine = db.create_engine(database)
    with TimedAction('Dropping SQL table'):
        engine.execute('DROP TABLE IF EXISTS ' + table)
    # Specialized insert methods, if available.
    if database.startswith('postgresql://'):
        # This gives about a 10x speedup over the default method.
        # ('multi' was about the same speed as the default.)
        sql_method=psql_insert_copy
    else:
        sql_method=None
    dtypes = {
        'timestamp': db.types.BigInteger,
        'username': db.types.String(32),
        'x': db.types.SmallInteger,
        'y': db.types.SmallInteger,
    }
    iterator = trange(0, len(dataset), STRIDE, desc='writing SQL',
                      unit_scale=STRIDE, unit='p')
    for i in iterator:
        dataset.iloc[i:i+STRIDE].to_sql(
            table, con=engine, method=sql_method, dtype=dtypes,
            if_exists='append')
    with TimedAction('Creating SQL index'):
        engine.execute('CREATE INDEX ix_' + table + '_coordinate ON ' + table + ' (x, y)')
        engine.execute('CREATE INDEX ix_' + table + '_byname ON ' + table + ' (username)')


def make_protobuf_header(dataset):
    year = dataset.ts[0].year
    header = place_pb2.Header()
    header.year = year
    user_hashes = dataset.username
    sorted_uh_index = dataset.username.groupby(dataset.username)\
                                      .transform('count')\
                                      .sort_values(ascending=False)\
                                      .index
    for username in dataset.username[sorted_uh_index].unique():
        header.user_hashes.append(username)
    for color_values in COLORS[year][:-1]:
        color = header.colors.add()
        color.r = color_values[0]
        color.g = color_values[1]
        color.b = color_values[2]
    return header


if __name__ == '__main__':
    main()
